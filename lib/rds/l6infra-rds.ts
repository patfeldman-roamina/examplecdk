import { Construct } from 'constructs';
import { InstanceType, SubnetSelection, Vpc } from 'aws-cdk-lib/aws-ec2';
import * as rds from 'aws-cdk-lib/aws-rds';
import * as ec2 from 'aws-cdk-lib/aws-ec2';

export class RDSConstruct extends Construct {
  constructor(scope: Construct, id: string, vpc: Vpc, subnets: SubnetSelection) {
    super(scope, id);

    const sqlServerInstance = new rds.DatabaseInstance(this, id, {
      // THESE TWO ARE FOR EXAMPLE, for faster loading
      engine: rds.DatabaseInstanceEngine.MYSQL,
      instanceType: ec2.InstanceType.of(ec2.InstanceClass.T2, ec2.InstanceSize.SMALL),

      // I THINK THAT YOU have to have a large for SQL SERVER
      // engine: rds.DatabaseInstanceEngine.SQL_SERVER_WEB,
      vpcSubnets: subnets,
      vpc: vpc,
      // backupRetention: Duration.days(7),
      // allowMajorVersionUpgrade: true,
      // storageEncrypted: true,
      // publiclyAccessible: false,
      // instanceIdentifier: id,
      // credentials: sqlCredentials,
      // allocatedStorage: 20,
      // securityGroups: [dbsg],
      // autoMinorVersionUpgrade: true,
      // instanceType: someInstanceType,
      // removalPolicy: RemovalPolicy.DESTROY,
      // monitoringInterval: Duration.seconds(60),
      // enablePerformanceInsights: true,
      // parameterGroup: someParameterGroup,
      // subnetGroup: someSubnetGroup,
      // preferredBackupWindow: someBackupWindow,
      // preferredMaintenanceWindow: somePreferredMaintenanceWindow,
    });
  }
}
