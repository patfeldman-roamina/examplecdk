import { Construct } from 'constructs';
import { Vpc } from 'aws-cdk-lib/aws-ec2';
import * as ec2 from 'aws-cdk-lib/aws-ec2';

export class SurveyConstruct extends Construct {
  constructor(scope: Construct, id: string, vpc: Vpc) {
    super(scope, id);

      const lookupMachineImage = new ec2.LookupMachineImage({
      name: 'name',
    
      filters: {
        filtersKey: ['Lime Survey'],
      },
      owners: ['whoeverownsthis'],
      // userData: userData,
      // windows: false,
    });
    const template = new ec2.LaunchTemplate(this, 'LaunchTemplate', {
      machineImage: lookupMachineImage,
      securityGroup: new ec2.SecurityGroup(this, 'LaunchTemplateSG', {
        vpc: vpc,
      }),
    });

  }
}
