import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import { VPCConstruct } from './vpc/l6infra-vpc';
import { RDSConstruct } from './rds/l6infra-rds';
// import { SurveyConstruct } from './lime/l6infra-lime';

export class L6InfraStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const vpcConstruct = new VPCConstruct(this, 'l6-dev-vpc');
    const rdsConstruct = new RDSConstruct(this, 'l6-dev-rds', vpcConstruct.vpc, vpcConstruct.rdsSubnets)    
    // const surveyConstruct = new SurveyConstruct(this, 'l6-dev-rds', vpcConstruct.vpc)    
  }
}
